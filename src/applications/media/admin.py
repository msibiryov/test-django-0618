from django.contrib import admin

from src.applications.media.models import Image, Video

# Register your models here.

admin.site.register([Image, Video])
