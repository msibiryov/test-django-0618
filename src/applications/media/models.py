from django.db import models

# Create your models here.
from django.db.models import CharField

from src.applications.recruiting.models import Qualification


class Image(models.Model):
    image = models.ImageField(upload_to="image")
    is_active = models.BooleanField(default=True)
    owner = models.ForeignKey(Qualification, on_delete=models.CASCADE)


class Video(models.Model):
    name = CharField(max_length=256, blank=False, editable=False)
    video = models.FileField(upload_to="video")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} (created at: {})'.format(self.name, self.created_at)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        self.name = self.video.name[self.video.name.rfind('/')+1:]

        super().save(force_insert, force_update, using, update_fields)
