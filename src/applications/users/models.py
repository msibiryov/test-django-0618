from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from src.applications.geo.models import Address

MALE = "M"
FEMALE = "F"
GENDER = (
    (MALE, _("Male")),
    (FEMALE, _("Female")),
)

DEUTSCH = "de"
ENGLISH = "en"
LANGUAGES = (
    (DEUTSCH, _("German")),
    (ENGLISH, _("English")),
)

APPLICANT_ROLE = "Applicant"
RECRUITER_ROLE = "Recruiter"
ROLES = (
    (APPLICANT_ROLE, _("Applicant")),
    (RECRUITER_ROLE, _("Recruiter"))
)


class Phone(models.Model):
    number = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.number


class Skill(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):

    username = None
    email = models.EmailField(_('E-mail address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email


class UFOUser(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    avatar = models.ImageField(upload_to='avatars')

    birthday = models.DateField(blank=True)

    profile_video = models.ForeignKey('media.Video', blank=False,
                                      related_name='owner',
                                      on_delete=models.PROTECT)

    gender = models.CharField(max_length=6, blank=False,
                              choices=GENDER, default=MALE)
    language = models.CharField(max_length=2, choices=LANGUAGES, blank=False,
                                default=DEUTSCH,
                                help_text=_('Default language.'))

    skype_id = models.CharField(max_length=50, blank=True)

    role = models.CharField(max_length=9, blank=False,
                            choices=ROLES, default=APPLICANT_ROLE)

    address = models.ForeignKey(Address, blank=False, related_name='owner',
                                on_delete=models.DO_NOTHING)
    phone = models.ForeignKey(Phone, blank=False, related_name='owner',
                              on_delete=models.PROTECT)

    is_active = models.BooleanField(default=True)
    enable_notifications = models.BooleanField(default=False)
    notes = models.CharField(max_length=1000, default="", blank=True)

    skills_list = models.ManyToManyField(Skill, blank=False)

    class Meta:
        verbose_name = 'UFO User'
        verbose_name_plural = 'UFO Users'

    def __str__(self):
        return self.user.email


class EditCVUFOUser(UFOUser):

    class Meta:
        proxy = True
        verbose_name = 'UFO User'
        verbose_name_plural = 'UFO Users'
