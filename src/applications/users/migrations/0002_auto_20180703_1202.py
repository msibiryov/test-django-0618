# Generated by Django 2.0.6 on 2018-07-03 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='extendedufouser',
            options={'verbose_name': 'UFO User', 'verbose_name_plural': 'UFO Users'},
        ),
        migrations.AlterField(
            model_name='phone',
            name='number',
            field=models.CharField(max_length=20, unique=True),
        ),
        migrations.AlterField(
            model_name='ufouser',
            name='skills_list',
            field=models.ManyToManyField(to='users.Skill', verbose_name='Skills list'),
        ),
    ]
