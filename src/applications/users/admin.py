from functools import update_wrapper

from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.admin import TabularInline
from django.http import HttpResponseRedirect
from django.urls import path, reverse
from django.utils.translation import ugettext_lazy as _

from src.applications.media.models import Video
from src.applications.recruiting.models import Qualification, \
    EducationQualification, JobQualification
from src.applications.users.forms import UFOUserAdminForm, \
    EditCVUFOUserAdminForm, UFOUserAddAdminForm
from src.applications.users.models import UFOUser, Skill, Phone, User, \
    EditCVUFOUser


class EducationInline(TabularInline):
    model = EducationQualification
    extra = 0
    min_num = 1

    exclude = ('qualification_type',)

    def get_queryset(self, request):
        queryset = super(EducationInline, self).get_queryset(request)
        return queryset.filter(qualification_type=Qualification.EDUCATION_TYPE)


class JobsInline(TabularInline):
    model = JobQualification
    extra = 0
    min_num = 1

    exclude = ('qualification_type',)

    def get_queryset(self, request):
        queryset = super(JobsInline, self).get_queryset(request)
        return queryset.filter(qualification_type=Qualification.JOB_TYPE)


class SkillsInline(TabularInline):
    model = UFOUser.skills_list.through
    extra = 0
    min_num = 1
    verbose_name_plural = 'Skills list'


class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


class UFOUserAdmin(admin.ModelAdmin):
    change_form = UFOUserAdminForm
    add_form = UFOUserAddAdminForm

    REQUIRED_USER_FIELDS = ['email', 'first_name', 'last_name']

    raw_id_fields = ('user', 'profile_video')

    radio_fields = {
        'role': admin.HORIZONTAL,
        'gender': admin.VERTICAL,
        'language': admin.HORIZONTAL,
    }

    inlines = [
        EducationInline,
        JobsInline,
        SkillsInline
    ]

    save_on_top = True

    change_fieldsets = (
        (None, {
            'fields': ['notes', ]}),
        ('Main info', {'fields': [
            'user',
            'avatar',
            'birthday',
            'profile_video',
            'role',
            'gender',
            'language',
        ]}),
        ('Contacts', {
            'classes': ('collapse',),
            'fields': [
                'skype_id',
                'address',
                'phone_number',
            ]}),
    )

    add_fieldsets = (
        (None, {
            'fields': ['notes', ]}),
        ('User', {'fields': [
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        ]}),
        ('UFOUser', {'fields': [
            'avatar',
            'birthday',
            'profile_video',
            'role',
            'gender',
            'language',
        ]}),
        ('Contacts', {
            'fields': [
                'skype_id',
                'address',
                'phone_number',
            ]}),
    )

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            self.form = self.add_form
            self.fieldsets = self.add_fieldsets
        else:
            self.form = self.change_form
            self.fieldsets = self.change_fieldsets

        form = super().get_form(request, obj=obj, **kwargs)

        if obj:
            form.base_fields['phone_number'].initial = obj.phone.number

        return form

    def save_form(self, request, form, change):
        obj = super().save_form(request, form, change)

        phone_number = form.cleaned_data['phone_number']

        if phone_number._state.adding:
            phone_number.save()
        obj.phone = phone_number

        if not change:
            user_data = {
                'email': form.cleaned_data['email'],
                'first_name': form.cleaned_data['first_name'],
                'last_name': form.cleaned_data['last_name'],
                'password': form.cleaned_data['password1'],
            }
            obj.user = User.objects.create_user(**user_data)

        return obj

    def get_urls(self):

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        editcv_admin = EditCVUFOUserAdmin(self.model, self.admin_site)

        urls = super().get_urls()

        opts = self.model._meta
        info = opts.app_label, opts.model_name

        ufo_user_urls = [
            path('<path:object_id>/editcv/',
                 wrap(editcv_admin.change_view),
                 name='%s_%s_editcv' % info),
        ]
        return ufo_user_urls + urls


class EditCVUFOUserAdmin(admin.ModelAdmin):
    REQUIRED_USER_FIELDS = ['email', 'first_name', 'last_name']

    form = EditCVUFOUserAdminForm

    fieldsets = (
        ('User', {'fields': REQUIRED_USER_FIELDS}),
        ('UFOUser', {'fields': [
            'avatar',
            'profile_video_file',
            'address',
            'phone_number',
        ]}),
    )

    filter_horizontal = ('skills_list', )

    inlines = [
        EducationInline,
        JobsInline,
        SkillsInline,
    ]

    change_form_template = 'admin/users/ufouser_editcv/change_form.html'

    def save_form(self, request, form, change):
        obj = super().save_form(request, form, change)

        phone_number = form.cleaned_data['phone_number']

        if phone_number._state.adding:
            phone_number.save()
        obj.phone = phone_number

        if form.cleaned_data['profile_video_file']:
            video = Video(video=form.cleaned_data['profile_video_file'])
            video.save()
            obj.profile_video = video

        user_data = {
            'email': form.cleaned_data['email'],
            'first_name': form.cleaned_data['first_name'],
            'last_name': form.cleaned_data['last_name'],
        }
        if not change:
            obj.user = User.objects.create_user(**user_data)
        else:
            for field in self.REQUIRED_USER_FIELDS:
                setattr(obj.user, field, form.cleaned_data.get(field))
            obj.user.save()

        return obj

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=obj, **kwargs)

        if obj:
            for field in self.REQUIRED_USER_FIELDS:
                form.base_fields[field].initial = getattr(obj.user, field)

            form.base_fields['phone_number'].initial = obj.phone.number

        return form

    def get_model_perms(self, request):
        return {}

    def changelist_view(self, request, extra_context=None):
        return HttpResponseRedirect(
            reverse('admin:%s_%s_changelist' % (
                self.model._meta.app_label,
                self.model._meta.model_name,
            ))
        )


admin.site.register(Phone)
admin.site.register(Skill)
admin.site.register(User, UserAdmin)
admin.site.register(UFOUser, UFOUserAdmin)
admin.site.register(EditCVUFOUser, EditCVUFOUserAdmin)

