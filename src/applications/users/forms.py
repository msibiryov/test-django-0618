from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.forms import CharField, FileField
from django.forms.widgets import Textarea, PasswordInput
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import FormMixin

from src.applications.users.models import EditCVUFOUser
from src.applications.users.models import UFOUser, Phone


def _clean_phone_number(obj):
    phone_number = obj.cleaned_data['phone_number']
    phone_obj = None

    try:
        phone_obj = Phone.objects.get(number=phone_number)
    except ObjectDoesNotExist:
        pass

    if not phone_obj:
        phone_obj = Phone(number=phone_number)

    return phone_obj


def _clean_email(obj):
    email_str = obj.cleaned_data['email']

    if obj.instance._state.adding and \
            get_user_model().objects.select_for_update().filter(email=email_str).exists():
        raise ValidationError(_("User with this e-mail already exists."))

    return email_str


class UFOUserAdminForm(forms.ModelForm):
    phone_number = CharField(max_length=20)

    class Meta:
        model = UFOUser
        fields = [
            'notes',
            'user',
            'avatar',
            'birthday',
            'profile_video',
            'role',
            'gender',
            'language',
            'skype_id',
            'address',
            'phone_number',
        ]
        widgets = {
            'notes': Textarea(attrs={
                'cols': '100',
                'rows': '2',
            }),
        }

    def clean_phone_number(self):
        return _clean_phone_number(self)


class UFOUserAddAdminForm(UFOUserAdminForm):
    email = CharField()
    first_name = CharField(max_length=100)
    last_name = CharField(max_length=100)
    password1 = CharField(max_length=100, widget=PasswordInput())
    password2 = CharField(max_length=100, widget=PasswordInput())

    class Meta:
        fields = [
            'notes',
            'email',
            'first_name', 'last_name',
            'password1', 'password2',
            'avatar',
            'birthday',
            'profile_video',
            'role',
            'gender',
            'language',
            'skype_id',
            'address',
            'phone_number',
        ]

    def clean(self):
        cleaned_data = super().clean()

        password = cleaned_data.get('password1')
        password_confirm = cleaned_data.get('password2')

        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(
                    "The two password fields must match."
                )
        return cleaned_data

    def clean_email(self):
        return _clean_email(self)


class EditCVUFOUserAdminForm(forms.ModelForm, FormMixin):
    email = CharField()
    first_name = CharField(max_length=100)
    last_name = CharField(max_length=100)

    phone_number = CharField(max_length=20)
    profile_video_file = FileField()

    class Meta:
        model = EditCVUFOUser
        exclude = ('phone', 'profile_video')
        widgets = {
            'notes': Textarea(),
        }

    def clean_phone_number(self):
        return _clean_phone_number(self)

    def clean_email(self):
        return _clean_email(self)

