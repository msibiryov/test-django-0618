from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=100, blank=False)

    class Meta:
        verbose_name_plural = _('Cities')

    def __str__(self):
        return self.name


class Address(models.Model):
    city = models.ForeignKey(City, blank=False, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = _('Addresses')

    def __str__(self):
        return self.city.name