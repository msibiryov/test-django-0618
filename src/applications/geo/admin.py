from django.contrib import admin
from src.applications.geo.models import Address, City

# Register your models here.

admin.site.register([Address, City])
