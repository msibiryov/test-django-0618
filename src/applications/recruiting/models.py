from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from src.applications.geo.models import City


class Qualification(models.Model):
    JOB_TYPE = "job"
    EDUCATION_TYPE = "education"
    QUALIFICATION_TYPE_DEFAULT = EDUCATION_TYPE

    QUALIFICATION_TYPES = (
        (JOB_TYPE, _("job")),
        (EDUCATION_TYPE, _("education"))
    )

    name = models.CharField(max_length=200, blank=False)
    level = models.CharField(max_length=50, blank=False)
    city = models.ForeignKey(City, blank=True, on_delete=models.DO_NOTHING)
    qualification_type = models.CharField(max_length=10,
                                          choices=QUALIFICATION_TYPES)
    owner = models.ForeignKey('users.UFOUser', on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):
        self.qualification_type = self.QUALIFICATION_TYPE_DEFAULT
        super().save(*args, **kwargs)

    def __str__(self):
        return '{}, {}, {}'.format(self.name, self.level, self.city)


class JobQualification(Qualification):
    QUALIFICATION_TYPE_DEFAULT = Qualification.JOB_TYPE

    class Meta:
        proxy = True
        verbose_name_plural = _('Job')


class EducationQualification(Qualification):
    QUALIFICATION_TYPE_DEFAULT = Qualification.EDUCATION_TYPE

    class Meta:
        proxy = True
        verbose_name_plural = _('Education')