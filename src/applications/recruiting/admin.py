from django.contrib import admin

# Register your models here.
from src.applications.recruiting.models import Qualification

admin.site.register(Qualification)
